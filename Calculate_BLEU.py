import math
import os
#-----------------------------------------------------------------------------------------------------------------------

candidate_file = "candidate.txt"
ref_files_directory_name = "ref_files"
ref_files = []
if os.path.isdir(ref_files_directory_name):
    for root, dirs, files in os.walk(ref_files_directory_name):
        for x in files:
            ref_files.append(os.path.join(root, x))
else:
    ref_files.append(ref_files_directory_name)

#-----------------------------------------------------------------------------------------------------------------------

BP = 0
R = 0

final_answer_list = []
list_of_lines_ref_list = []
list_of_lines_candidate = [list.rstrip('\n') for list in open(candidate_file,'r',encoding="utf-8")]

for filename in ref_files:
    list_of_lines_ref = [list.rstrip('\n') for list in open(filename,'r',encoding="utf-8")]
    list_of_lines_ref_list.append(list_of_lines_ref)

numerator_list = []
denominator_list = []

numerator_unigram = 0
denominator_unigram = 0

numerator_bigram = 0
denominator_bigram = 0

numerator_trigram = 0
denominator_trigram = 0

numerator_fourgram = 0
denominator_fourgram = 0

for i in range(len(list_of_lines_candidate)):

    ref_token_list = []
    for complete_ref_list in list_of_lines_ref_list:
        ref_token_list.append(complete_ref_list[i].strip().split())

    ref_counts_list_unigram = [] #list of dictionaries
    candidate_count_unigram = {}
    reference_count_max_unigram = {}

    ref_counts_list_bigram = []  # list of dictionaries
    candidate_count_bigram = {}
    reference_count_max_bigram = {}

    ref_counts_list_trigram = []  # list of dictionaries
    candidate_count_trigram = {}
    reference_count_max_trigram = {}

    ref_counts_list_fourgram = []  # list of dictionaries
    candidate_count_fourgram = {}
    reference_count_max_fourgram = {}


    candidate_token_list = list_of_lines_candidate[i].strip().split();

    denominator_unigram += len(candidate_token_list)

    # Unigram starts here!
    #------------------------------------------------------------------------------------------------------------------------------
    for word in candidate_token_list:
        if word in candidate_count_unigram:
            value = candidate_count_unigram[word] + 1
            candidate_count_unigram[word]=value
        else:
            candidate_count_unigram[word] = 1


    for i in ref_token_list:
        dict = {}
        for j in i:
            if j in dict:
                dict[j]+=1
            else:
                dict[j]=1
        ref_counts_list_unigram.append(dict)

    for word in candidate_token_list:
        for dict in ref_counts_list_unigram:
            for word in dict:
                if word in reference_count_max_unigram:
                    if dict[word]>reference_count_max_unigram[word]:
                        reference_count_max_unigram[word]=dict[word]
                else:
                    reference_count_max_unigram[word]=dict[word]

    word_wise_value_list = {}

    for key in candidate_count_unigram:
        if key in reference_count_max_unigram:
            min_val = min(candidate_count_unigram[key],reference_count_max_unigram[key])
            word_wise_value_list[key] = min_val

    numerator_unigram += sum(word_wise_value_list.values())

    #BP Calculation
    #--------------------------------------------------------------------------------------------------------------------------------
    candidate_len = len(candidate_token_list)
    ref_lengths = []
    for reflist in ref_token_list:
        ref_lengths.append(len(reflist))

    min_diff = abs(candidate_len - ref_lengths[0])
    index = 1
    while index<len(ref_lengths):
        if abs(candidate_len-ref_lengths[index]) < min_diff:
            min_diff = abs(candidate_len-ref_lengths[index])
        index+=1

    final_ref = 0

    for i in ref_lengths:
        if abs(candidate_len - i) == min_diff:
            final_ref = i

    R+=final_ref

    #---------------------------------------------------------------------------------------------------------------------------------
    # # bigram starts here
    bigram_token_list = []
    e=0
    f=1

    while f < len(candidate_token_list):
        bigram_token_list.append(candidate_token_list[e]+" "+candidate_token_list[f])
        e+=1
        f+=1

    # print("Bigram denominator ",len(bigram_token_list))
    denominator_bigram+=len(bigram_token_list)

    ref_token_list_bigram = []
    for i in ref_token_list:
        e=0
        f=1
        intermediate_token_list = []
        while f < len(i):
            intermediate_token_list.append(i[e] + " " + i[f])
            e += 1
            f += 1
        ref_token_list_bigram.append(intermediate_token_list)

    for word in bigram_token_list:
        if word in candidate_count_bigram:
            value = candidate_count_bigram[word] + 1
            candidate_count_bigram[word]=value
        else:
            candidate_count_bigram[word] = 1


    for i in ref_token_list_bigram:
        dict = {}
        for j in i:
            if j in dict:
                dict[j]+=1
            else:
                dict[j]=1
        ref_counts_list_bigram.append(dict)

    for word in bigram_token_list:
        for dict in ref_counts_list_bigram:
            for word in dict:
                if word in reference_count_max_bigram:
                    if dict[word]>reference_count_max_bigram[word]:
                        reference_count_max_bigram[word]=dict[word]
                else:
                    reference_count_max_bigram[word]=dict[word]

    word_wise_value_list = {}

    for key in candidate_count_bigram:
        if key in reference_count_max_bigram:
            min_val = min(candidate_count_bigram[key],reference_count_max_bigram[key])
            word_wise_value_list[key] = min_val

    numerator_bigram += sum(word_wise_value_list.values())
    #------------------------------------------------------------------------------------------------------------------------------
    #trigram starts here
    trigram_token_list = []
    e = 0
    f = 2

    while f < len(candidate_token_list):

        trigram_token_list.append(candidate_token_list[e] + " " + candidate_token_list[e+1] + " " + candidate_token_list[f])
        e += 1
        f += 1

    denominator_trigram+=len(trigram_token_list)

    ref_token_list_trigram = []
    for i in ref_token_list:
        e = 0
        f = 2
        intermediate_token_list = []
        while f < len(i):
            intermediate_token_list.append(i[e] + " " + i[e+1] + " " + i[f])
            e += 1
            f += 1
        ref_token_list_trigram.append(intermediate_token_list)

    for word in trigram_token_list:
        if word in candidate_count_trigram:
            value = candidate_count_trigram[word] + 1
            candidate_count_trigram[word] = value
        else:
            candidate_count_trigram[word] = 1

    for i in ref_token_list_trigram:
        dict = {}
        for j in i:
            if j in dict:
                dict[j] += 1
            else:
                dict[j] = 1
        ref_counts_list_trigram.append(dict)

    for word in trigram_token_list:
        for dict in ref_counts_list_trigram:
            for word in dict:
                if word in reference_count_max_trigram:
                    if dict[word] >= reference_count_max_trigram[word]:
                        reference_count_max_trigram[word] = dict[word]
                else:
                    reference_count_max_trigram[word] = dict[word]

    word_wise_value_list = {}
    #
    for key in candidate_count_trigram:
        if key in reference_count_max_trigram:
            min_val = min(candidate_count_trigram[key],reference_count_max_trigram[key])
            word_wise_value_list[key] = min_val

    numerator_trigram += sum(word_wise_value_list.values())
    #-----------------------------------------------------------------------------------------------------------------------------------
    #4-gram starts here
    fourgram_token_list = []
    e = 0
    f = 3

    while f < len(candidate_token_list):
        fourgram_token_list.append(
            candidate_token_list[e] + " " + candidate_token_list[e + 1] + " " + candidate_token_list[e + 2] + " " + candidate_token_list[f])
        e += 1
        f += 1

    # print("Fourgram denominator ", len(fourgram_token_list))
    denominator_fourgram+=len(fourgram_token_list)
    ref_token_list_fourgram = []
    for i in ref_token_list:
        e = 0
        f = 3
        intermediate_token_list = []
        while f < len(i):
            intermediate_token_list.append(i[e] + " " + i[e + 1] + " " + i[e+2] + " " + i[f])
            e += 1
            f += 1
        ref_token_list_fourgram.append(intermediate_token_list)

    for word in fourgram_token_list:
        if word in candidate_count_fourgram:
            value = candidate_count_fourgram[word] + 1
            candidate_count_fourgram[word] = value
        else:
            candidate_count_fourgram[word] = 1

    for i in ref_token_list_fourgram:
        dict = {}
        for j in i:
            if j in dict:
                val = dict[j] + 1
                dict[j]=val
            else:
                dict[j] = 1
        ref_counts_list_fourgram.append(dict)

    for word in fourgram_token_list:
        for dict in ref_counts_list_fourgram:
            for word in dict:
                if word in reference_count_max_fourgram:
                    if dict[word] > reference_count_max_fourgram[word]:
                        reference_count_max_fourgram[word] = dict[word]
                else:
                    reference_count_max_fourgram[word] = dict[word]

    word_wise_value_list = {}

    for key in candidate_count_fourgram:
        if key in reference_count_max_fourgram:
            min_val = min(candidate_count_fourgram[key], reference_count_max_fourgram[key])
            word_wise_value_list[key] = min_val

    numerator_fourgram += sum(word_wise_value_list.values())
#----------------------------------------------------------------------------------------------------------------------------
numerator_list.append(numerator_unigram)
denominator_list.append(denominator_unigram)

numerator_list.append(numerator_bigram)
denominator_list.append(denominator_bigram)

numerator_list.append(numerator_trigram)
denominator_list.append(denominator_trigram)

numerator_list.append(numerator_fourgram)
denominator_list.append(denominator_fourgram)
division_answer_list = []

for w in range(len(numerator_list)):
    division_answer_list.append(numerator_list[w]/denominator_list[w])

print(division_answer_list)

multiplication = 1

print(numerator_list)
print(denominator_list)

for x in division_answer_list:
    multiplication *= x

geometric_mean = math.pow(multiplication,0.25)

C = denominator_list[0]
print(C)
print(R)

one_minus_r_by_c = 1 - (R/C)
if(C>R):
    BP = 1
else:
    BP = math.exp(one_minus_r_by_c)

BLEU = BP * geometric_mean

print(BLEU)

output = open('bleu_out.txt','w',encoding="utf-8")
output.write(str(BLEU))
output.close()